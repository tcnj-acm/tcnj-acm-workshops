# Webscraper tutorial
# craigslist.py
# tutorial slides found at:

import requests
from bs4 import BeautifulSoup, SoupStrainer
import bs4

#get the posting title
def getPostingTitle(soup):
	posting_title = soup.find_all('h2', {'class': 'postingtitle'})
	print posting_title[0].text
	title = posting_title[0].text
	return title

# make the http request to the craigslist server and pass to the get method
try:
	http_request = requests.get('http://newyork.craigslist.org/search/hhh?query=apartment&sort=rel').text
	html_soup = BeautifulSoup(http_request, 'html.parser')
	individual_links = html_soup.find_all('a', {'class' : 'i'})

# loop through each link and extract the post title
	for n in individual_links:
		# visit each individual ad link in the results page
		link = 'http://newyork.craigslist.org' + n['href']
		individual_ad = requests.get(link).text
		html_soup_2 = BeautifulSoup(individual_ad, 'html.parser')
		ad_title = getPostingTitle(html_soup_2)

except requests.exceptions.RequestException as e:
	print "there was an error with the connection. Please try again."
	print e