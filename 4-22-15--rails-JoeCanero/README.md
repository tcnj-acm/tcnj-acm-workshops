# rails-workshop
Rails resources for the workshop ran by Joe Canero on 4/22/15.

The rails tutorial goes through the process of making an Amazon clone. 

In order to benefit fully from the tutorial, install ruby and be sure to run 
```ruby
gem install rails
```
There are sometimes issues with the rails install revolving around nokogiri. If you run into these errors, a quick google search will return some helpful results. If you get no errors, then lucky you! :)

Once 
```ruby
gem install rails
``` 
is successful, you're fully ready to participate... Just clone this repository!
