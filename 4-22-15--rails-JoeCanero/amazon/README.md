
<h1>What's been covered so far in the tutorial</h1>
<h2>Basic Rails Info</h2>
<ul>
  <li>Rails is an open source web framework excellent for developing applications in an agile fashion. </li>
  <li>Rails gets its speed from a concept called “convention over configuration”</li>
  <li>doing things “The Rails Way” lets progress be made at an extraordinary pace</li>
  <li>Rails is great for developing applications that fall under an archetype known as CRUD: “create, read, update, destroy”</li>
  <li>trying to break convention and do something really weird might be annoying and mean more work for you.</li>
  <li>Rails follows the design paradigm called MVC: model-view-controller</li>
  <li>The model is basically our application data, such as database tables.</li>
  <li>The controller is the component that updates the model and also tells the view when and how to present the model.</li>
  <li>The view is a visual representation of the model and it is backed by controller commands.</li>
</ul>
<h2>Tutorial</h2>
<ul>
  <li>run “rails new amazon” to generate a new rails project in the current directory called amazon.</li>
  <li>run "rails server" and keep the server open in another terminal window</li>
  <li>Go to localhost:3000 to see the Rails welcome page.</li>
  <li>run “rails generate scaffold Product name:string description:string
    <ul>
      <li>the first thing we see listed is active_record, which is basically rails’ way of interacting with a database. active_record abstracts specific database commands, such as those for mySQL or postgres, so your code can be database agnostic. rails created a model for us, under app/models/product.rb and also a database migration, which is basically a way to incrementally update your database.</li>
      <li>the next thing we see is a route, called resources :products, which basically created a bunch of URLs for us so we can do things with our products.</li>
      <li>next, we see rails has created a controller for us, under app/controllers/products.rb</li>
      <li>rails also made a bunch of views for us that correspond to the different routes.</li>
      <li>we also see that rails made some assets for us, which are basically css and javascript files for products-related views. javascript is written in coffeescript, and css is written in scss.</li>
    </ul>
  </li>
  <li>now, let’s familiarize ourself with another command line tool, Rake, which is basically a tool to run different tasks that rails doesn’t do explicitly. since we saw that rails made routes for us, let’s run “rake routes” to see a list of routes.
    <ul>
      <li>the output shows the URI pattern we can type to access those specific routes, the controller action that a route maps to, the appropriate HTTP verb, and prefix defines a function we can use in our code to access the route.</li>
    </ul>
  </li>
  <li>if we navigate to localhost:3000/products, we will run into an error. rails tells us migrations are pending, and we didn’t run the one that was generated with our scaffold. another rake task that we can use is called “rake db:migrate”, which runs all outstanding database migrations in order to update our database, by creating new tables, columns, or whatever.</li>
  <li>if we go back to localhost:3000/products, we can see an empty list of products.</li>
  <li>if we click on new product, it will take us to a form where we can make a new product.
    <ul>
      <li>all of this is generated as part of the scaffolding system, and we get a lot of functionality for very little effort.</li>
      <li>make a new product</li>
    </ul>
  </li>
  <li>now, if we click on our URL bar in our browser, we can see that it reads http://localhost:3000/products/1.
    <ul>
      <li>if you run rake routes again, this url maps to the 5th route, /products/:id, where id = 1.</li>
      <li>this maps to the “show” action of our products controller.</li>
      <li>if we navigate back to localhost:3000/products, which, if you remember the routes list, corresponds to the index action of our controller, we can see a nice product list beginning to form.</li>
    </ul>
  </li>
  <li>if you navigate back to our root url, localhost:3000, you’ll see we still have our rails welcome page, which isn’t appropriate for our app. let’s make a new welcome page.</li>
  <li>open the amazon directory in your text editor of choice.</li>
  <li>in your text editor, open config/routes.rb. uncomment line 7.</li>
  <li>if you refresh the url at localhost:3000, we’ll get an error telling us we haven’t made a welcome controller. rails knows we need a welcome controller because the root we just uncomment says to map the root to “welcome#index”, which is the index method of the welcome controller.</li>
  <li>let’s fix the error. in your command line, run “rails generate controller welcome” and see a bunch of output. one of first things made was a new controller, app/controllers/welcome.rb</li>
  <li>open app/controllers/welcome.rb and see an empty ruby class declaration for the WelcomeController, which is a subclass of Rails’ controller base class, ApplicationController.</li>
  <li>make a method called "index"</li>
  <li>once the index method is made, let’s refresh our browser at localhost:3000 and see another error. now it is telling us it is missing a template for welcome/index. this is basically saying that we do not have a view file for the controller action we just made, or in other words, we need to make an html file that we can display.</li>
  <li>make a new file in the directory app/views/welcome called index.html.erb. the name of the file is index because it corresponds to the route we made, it’s an html file, and it also has an erb extension, which stands for embedded ruby, meaning we can write ruby code write in our html file.</li>
  <li>let’s make our html file. put \<h1>Welcome to Amazon!\</h1> in the file and refresh our browser. you should see the welcome page.</li>
  <li>now let’s see what embedded ruby actuallu is. let’s add the following to our html file "\<p>Greetings, <%= @name %>\</p>". The syntax for embedding ruby is as follows you put your ruby code between <%= and %> if you want the code to output to the page, and you it between <% and %> if the code should not output anything, and you would do this for the start and end of loops, for example.</li>
  <li>the ruby code we wrote is just going to display the value of our instance variable @name on the page. @ symbols in front of variable names mean it is an instance variable.</li>
  <li>inside our index method, add @name = “YOUR NAME”. instance variables are automatically available in the html page that the action maps to.</li>
  <li>let’s do some more with the embedded ruby and use a very useful rails function, called link_to.
    <ul>
      <li>Reopen app/views/welcome/index.html.erb</li>
      <li>Add this: \<p>See our <%= link_to "products", products_path %>\</p></li>
      <li>The first argument to the function is just going to be the name of the link, and the second argument is where the link should go. If we look back at our list of routes, you can see that the first route has a prefix of products. if we add "_path" after the prefix, we get the rails method we used in our html file that takes us to the corresponding URL.</li>
      <li>to get more practice with this, let’s add another route. \<p>Selling something? Create a new <%= link_to "product", new_product_path %>\</p></li>
    </ul>
  </li>
<ul>
